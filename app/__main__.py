import os
import subprocess
import pandas as pd
import logging

from pandas import DataFrame

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def run_spider(spider_name):

    process = subprocess.run(["scrapy", "crawl", spider_name], cwd=os.path.join(PROJECT_PATH, "app"))

    return process.returncode


if __name__ == "__main__":

    csv_path = os.path.join(PROJECT_PATH, "output", "csv" )
    # List of CSV file names
    csv_files = ['borsino', 'tempocasa', 'tecnocasa']
    
    for file in csv_files:
        run_spider(file)

    # Create an empty excel file
    writer = pd.ExcelWriter(os.path.join(PROJECT_PATH, 'output', 'excel', 'output.xlsx'))
    
    logger.info("Reading CSV files")
    
    borsino_pd = pd.read_csv(os.path.join(csv_path, f"{csv_files[0]}.csv"))
    tempocasa_pd = pd.read_csv(os.path.join(csv_path, f"{csv_files[1]}.csv"))
    tecnocasa_pd = pd.read_csv(os.path.join(csv_path, f"{csv_files[2]}.csv"))
    
    logger.info("CSV files read")

    # Add to borsino a new column tempocasa and populate it with cell in TEMPOCASA column from tempocasa sheet if value in CITTA' column from tempocasa is present in borsino sheet
    borsino_pd["TEMPOCASA"] = borsino_pd["CITTA'"].apply(lambda x: tempocasa_pd.loc[tempocasa_pd["CITTA'"] == x, "TEMPOCASA"].values[0] if x in tempocasa_pd["CITTA'"].values else None)
    borsino_pd["TECNOCASA"] = borsino_pd["CITTA'"].apply(lambda x: tecnocasa_pd.loc[tecnocasa_pd["CITTA'"] == x, "TECNOCASA"].values[0] if x in tecnocasa_pd["CITTA'"].values else None)
    
    # Todo: Update else and remove that row
    tempocasa_pd["Presente in borsino"] = tempocasa_pd["CITTA'"].apply(lambda x: 'no' if x not in borsino_pd["CITTA'"].values else 'si')
    tecnocasa_pd["Presente in borsino"] = tecnocasa_pd["CITTA'"].apply(lambda x: 'no' if x not in borsino_pd["CITTA'"].values else 'si')
    
    # remove rows with si in Presente in borsino column
    tempocasa_pd = tempocasa_pd[tempocasa_pd["Presente in borsino"] == 'no']
    tecnocasa_pd = tecnocasa_pd[tecnocasa_pd["Presente in borsino"] == 'no']
    
    logger.info("Writing to excel")
    
    # To excel removing index
    borsino_pd.to_excel(writer, sheet_name='Borsino', index=False)
    tempocasa_pd.to_excel(writer, sheet_name='Tempocasa', index=False)
    tecnocasa_pd.to_excel(writer, sheet_name='Tecnocasa', index=False)
    
    logger.info("Excel written")
    # Save the excel file
    writer._save()
    
    logger.info("Excel saved")
    

    