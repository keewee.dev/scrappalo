# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst

class PropertyItem(scrapy.Item):
    region = scrapy.Field(output_processor=TakeFirst())
    province = scrapy.Field(output_processor=TakeFirst())
    city = scrapy.Field(output_processor=TakeFirst())
    andamento_vendite_residue = scrapy.Field(output_processor=TakeFirst())
    numero_abitazioni = scrapy.Field(output_processor=TakeFirst())
    numero_abitanti = scrapy.Field(output_processor=TakeFirst())
    tecnocasa = scrapy.Field(output_processor=TakeFirst())
    tempocasa = scrapy.Field(output_processor=TakeFirst())
    remax = scrapy.Field(output_processor=TakeFirst())

class TecnoCasaItem(scrapy.Item):
    province = scrapy.Field(output_processor=TakeFirst())
    city = scrapy.Field(output_processor=TakeFirst())
    tecnocasa = scrapy.Field(output_processor=TakeFirst())