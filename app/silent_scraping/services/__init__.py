import requests
from bs4 import BeautifulSoup


def get_remax_value(city) -> int | None:
    try:
        url = f"https://www.remax.it/trova/agenti-agenzie/cerca?kind=agencies&location={city}&location_type=municipalities&name"

        response = requests.get(url)

        html_content = response.content

        # print(html_content)
        # assuming that the HTML content is stored in a variable called 'html_content'
        soup = BeautifulSoup(html_content, "html.parser")
        # check if there is a p tag with text that contains: La ricerca non ha prodotto risultati.
        p_tag = soup.find("p", text="La ricerca non ha prodotto risultati, perciò è stata estesa.")
        if p_tag:
            return 0
        else:
            # find the span tag with class 'head-filters__mini-count'
            span_tag = soup.find("span", class_="head-filters__mini-count")

            # get the text inside the strong tag
            result_count = int(span_tag.strong.text)
            return result_count
    except:
        return None


def get_andamento_vendite_residue(response) -> int | None:
    try:
        andamento_vendite_residue = response.css(
            'div.text-2:contains("And. Vendite Resid.") span.pull-right.font-weight-bold span.text-danger.text-1::text'
        ).get()
        # Adjust the values to be integers
        return int(andamento_vendite_residue.split(" ")[0].replace(" ", ""))
    except:
        return None


def get_numero_abitazioni(response) -> int | None:
    try:
        numero_abitazioni = response.css(
            'div.text-2:contains("Numero Abitazioni") span.pull-right span.font-weight-bold::text'
        ).get()
        return int(numero_abitazioni.replace(" ", ""))
    except:
        return None


def get_numero_abitanti(response) -> int | None:
    try:
        numero_abitanti = response.css(
            'div.text-2:contains("Numero Abitanti") span.pull-right.font-weight-bold::text'
        ).get()

        return int(numero_abitanti.replace(" ", ""))
    except:
        return None
