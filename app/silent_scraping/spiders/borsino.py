import os
import scrapy

from ..items import PropertyItem
from ..services import (
    get_andamento_vendite_residue,
    get_numero_abitazioni,
    get_numero_abitanti,
    get_remax_value,
)

all_italy_regions = [
    "abruzzo",
    "basilicata",
    "calabria",
    "campania",
    "emilia-romagna",
    "friuli-venezia-giulia",
    "lazio",
    "liguria",
    "lombardia",
    "marche",
    "molise",
    "piemonte",
    "puglia",
    "sardegna",
    "sicilia",
    "toscana",
    "trentino-alto-adige",
    "umbria",
    "valle-d-aosta",
    "veneto",
]

region_names = all_italy_regions
url_prefix = "https://borsinoimmobiliare.it/quotazioni-immobiliari/"

out_file = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))),
    "output",
    "csv",
    "borsino.csv",
)

class Borsino(scrapy.Spider):
    name = __name__.split(".")[-1]
    start_urls = [f"{url_prefix}{region}/" for region in region_names]
    custom_settings = {
        "LOG_LEVEL": "INFO",
    }
    handle_httpstatus_list = [404]

    def __init__(self):
        with open(out_file, "w") as f:
            # Header
            f.write(
                "REGIONE,PROV.,CITTA',NTN.,N. CONTATTI,N. ABITANTI,REMAX\n"
            )

    def parse(self, response):
        links = response.css("table.table tbody tr td a::attr(href)").getall()
        filtered_links = [
            link
            for link in links
            if link.startswith("https://borsinoimmobiliare.it/quotazioni-immobiliari/")
            and len(link.split("/")) == 7
        ]
        for link in filtered_links:
            yield scrapy.Request(link, callback=self.parse_region)

    def parse_region(self, response):
        links = response.css("table.table tbody tr td a::attr(href)").getall()
        filtered_links = [
            link
            for link in links
            if link.startswith("https://borsinoimmobiliare.it/quotazioni-immobiliari/")
            and len(link.split("/")) == 8
        ]
        for link in filtered_links:
            yield scrapy.Request(link, callback=self.parse_province)

    def parse_province(self, response):
        andamento_vendite_residue = get_andamento_vendite_residue(response)
        numero_abitazioni = get_numero_abitazioni(response)
        numero_abitanti = get_numero_abitanti(response)
        region = response.url.split("/")[4]
        city = str(response.url.split("/")[6])
        province = str(response.url.split("/")[5].replace("-provincia", ""))

        remax = get_remax_value(city)
        remax = 0 if remax > 300 else remax

        item = PropertyItem(
            region=region,
            province=province,
            city=city,
            andamento_vendite_residue=andamento_vendite_residue,
            numero_abitazioni=numero_abitazioni,
            numero_abitanti=numero_abitanti,
            remax=remax,
        )

        with open(out_file, "a") as f:
            f.write(
                f"{item['region']},{item['province']},{item['city']},{item['andamento_vendite_residue']},{item['numero_abitazioni']},{item['numero_abitanti']},{item.get('remax', None)}\n"
            )
