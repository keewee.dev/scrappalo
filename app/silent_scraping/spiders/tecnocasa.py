import os
import scrapy
from scrapy.signals import spider_closed

url_prefix = "https://www.tecnocasa.it/agenzie.html"

data = {}
out_file = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))),
    "output",
    "csv",
    "tecnocasa.csv",
)

class TecnoCasa(scrapy.Spider):
    name = __name__.split(".")[-1]
    start_urls = [url_prefix]
    custom_settings = {
        "LOG_LEVEL": "INFO",
    }
    handle_httpstatus_list = [404]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with open(out_file, "w") as f:
            # Header
            f.write(
                "PROV.,CITTA',TECNOCASA\n"
            )

    def start_requests(self):
        self.crawler.signals.connect(self.on_spider_closed, signal=spider_closed)
        return super().start_requests()

    def parse(self, response):
        self.logger.info(f"Response status: {response.url} {response.status}")
        if response.status == 404:
            # retry the request if contains "d-" in the url
            self.logger.info(f"Skipping {response.url}")
        else:
        # Copy every href inside li elements inside all divs with class "agency-province-blocks"
            for href in response.xpath('//div[@class="agency-province-blocks"]//li/a/@href'):
             
                link = response.urljoin(href.extract())
                yield response.follow(link, self.re_parse)
                    
                
    def re_parse(self, response):
        # self.logger.info(f"Response status: {response.url} {response.status}")
        if response.status == 404:
            # retry the request if contains "d-" in the url
            self.logger.info(f"Skipping {response.url}")
        else:
        # Copy every href inside li elements inside all divs with class "agency-province-blocks"
            # self.logger.info(f"Processing {response.url}")
            hrefs = response.xpath('//div[@class="tab-agency-list"]//a/@href')
            links = [response.urljoin(href.extract()) for href in hrefs]
            for link in links:
                # self.logger.info(f"Processing {link}")
                province = link.split("/")[-3]
                city = link.split("/")[-2]
                if province not in data.keys():
                    data[province] = {
                        city: 1
                    }
                elif city not in data[province].keys():
                    data[province][city] = 1
                else:
                    data[province][city] += 1
            # self.logger.info(data)
    
    def on_spider_closed(self, spider):
        self.logger.info("Spider closed")
        for province in data.keys():
            for city in data[province].keys():
                with open(out_file, "a") as f:
                    f.write(
                        f"{province},{city},{data[province][city]}\n"
                    )
        self.logger.info("File written")

            
                


