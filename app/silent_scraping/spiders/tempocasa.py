import os
import scrapy
from scrapy.signals import spider_closed


url_prefix = "https://tempocasa.it/it/agenzie-immobiliari"

out_file = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))),
    "output",
    "csv",
    "tempocasa.csv",
)
data = {}

class Tempocasa(scrapy.Spider):
    name = __name__.split(".")[-1]
    start_urls = [url_prefix]
    custom_settings = {
        "LOG_LEVEL": "INFO",
    }
    handle_httpstatus_list = [404]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with open(out_file, "w") as f:
            # Header
            f.write(
                "PROV.,CITTA',TEMPOCASA\n"
            )

    def start_requests(self):
        self.crawler.signals.connect(self.on_spider_closed, signal=spider_closed)
        return super().start_requests()

    def parse(self, response):
        self.logger.info(f"Response status: {response.url} {response.status}")
        if response.status == 404:
            # retry the request if contains "d-" in the url
            self.logger.info(f"Skipping {response.url}")
        else:
        # Copy every href inside a elements without class greentitle inside all divs with class "test"
            for href in response.xpath('//div[@class="test"]//a[not(contains(@class, "greentitle"))]/@href'):
                
                link = response.urljoin(href.extract())
                # self.logger.info(f"Processing {link}")
                yield response.follow(link, self.re_parse)
                    
                    
                
    def re_parse(self, response):
        # self.logger.info(f"Response status: {response.url} {response.status}")
        if response.status == 404:
            # retry the request if contains "d-" in the url
            self.logger.info(f"Skipping {response.url}")
        else:
        # Copy every href inside li elements inside all divs with class "agency-province-blocks"
            # self.logger.info(f"Processing {response.url}")
            divs = response.xpath('//div[@class="provincie-container"]//div//div')
            for div in divs:
                a = div.xpath('./following-sibling::a[1]')
                a_href = a.xpath('./@href').extract_first()
                div_text = div.xpath('./text()').extract_first()

                if a_href is not None:
                    self.logger.info(f"Processing {a_href}")
                    self.logger.info(f"Processing {div_text}")
                    # if a_href.split("/")[-1] starts with a number
                    if a_href.split("/")[-1][0].isdigit():
                        province = a_href.split("/")[-3]
                        city = a_href.split("/")[-2]
                    else:
                        province = a_href.split("/")[-2]
                        city = a_href.split("/")[-1]
                    if province not in data.keys():
                        data[province] = {
                            city: div_text
                        }
                    else:
                        data[province][city] = div_text
                        

    
    def on_spider_closed(self, spider):
        self.logger.info("Spider closed")
        for province in data.keys():
            for city in data[province].keys():
                with open(out_file, "a") as f:
                    f.write(
                        f"{province},{city},{data[province][city]}\n"
                    )
        self.logger.info("File written")

            
                


